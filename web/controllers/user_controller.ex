defmodule TechTalents.UserController do
  use TechTalents.Web, :controller

  alias TechTalents.User

  plug Guardian.Plug.EnsureAuthenticated, [handler: TechTalents.SessionController] when action in [:rooms]

  def create(conn, 
    %{
      "data" => %{
      "type" => "users",
        "attributes" => %{
          "username" => username,
          "email" => email,
          "password" => password
        }
      }
    }) do

    changeset = User.registration_changeset %User{}, %{
      username: username,
      email: email,
      password: password
    }

    case Repo.insert(changeset) do
      {:ok, user} ->
        new_conn = Guardian.Plug.api_sign_in(conn, user, :access)
        jwt = Guardian.Plug.current_token(new_conn)

        new_conn
        |> put_status(:created)
        |> render(TechTalents.SessionView, "show.json", user: user, jwt: jwt)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(TechTalents.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def rooms(conn, _params) do
    current_user = Guardian.Plug.current_resource(conn)
    rooms = Repo.all(assoc(current_user, :rooms))
    render(conn, TechTalents.RoomView, "index.json", %{rooms: rooms})
  end
end