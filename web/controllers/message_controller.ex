defmodule TechTalents.MessageController do
  use TechTalents.Web, :controller

  plug Guardian.Plug.EnsureAuthenticated, handler: TechTalents.SessionController

  def index(conn, params) do
    last_seen_id = params["last_seen_id"] || 0
    room = Repo.get!(TechTalents.Room, params["room_id"])

    page =
      TechTalents.Message
      |> where([m], m.room_id == ^room.id)
      |> where([m], m.id < ^last_seen_id)
      |> order_by([desc: :inserted_at, desc: :id])
      |> preload(:user)
      |> TechTalents.Repo.paginate()

    render(conn, "index.json", %{messages: page.entries, pagination: TechTalents.PaginationHelpers.pagination(page)})
  end
end
