defmodule TechTalents.SessionControllerTest do
  use TechTalents.ConnCase

  alias TechTalents.User

  @user_attrs %{
    username: "cristiano23",
    email: "cristiano.codelab@gmail.com",
    password: "ph03nixd3v3l0p3r"
  }

  @login_attrs %{email: @user_attrs.email, password: @user_attrs.password}

  @invalid_attrs %{}

  setup %{conn: conn} do
    create_user
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  def create_user do
    changeset = User.registration_changeset(%User{}, @user_attrs)
    TechTalents.Repo.insert(changeset)
  end

  test "creates a session with registered user", %{conn: conn} do
    conn = post conn, session_path(conn, :create), 
      %{grant_type: "password", attributes: @login_attrs}

    assert json_response(conn, 201)["data"]["id"]
    assert json_response(conn, 201)["data"]["username"]
    assert json_response(conn, 201)["data"]["email"]
    assert json_response(conn, 201)["meta"]["token"]
  end
end