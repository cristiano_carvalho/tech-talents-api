defmodule TechTalents.RoomControllerTest do
  use TechTalents.ConnCase

  alias TechTalents.Room
  alias TechTalents.User

  @user_attrs %{
    username: "cristiano23",
    email: "cristiano.codelab@gmail.com",
    password: "ph03nixd3v3l0p3r"
  }

  @login_attrs %{email: @user_attrs.email, password: @user_attrs.password, token: "123"}

  @valid_attrs %{name: "some content", topic: "some content"}
  @invalid_attrs %{}

  def create_user do
    changeset = User.registration_changeset(%User{}, @user_attrs)
    Repo.insert changeset
  end

  setup %{conn: conn} do
    user = create_user
    # conn = post conn, session_path(conn, :create), 
    #   %{grant_type: "password", attributes: @login_attrs}

    conn =
      conn
      |> put_req_header("authorization", "Bearer #{user.token}")
      |> put_req_header("accept", "application/json")
    {:ok, conn: conn}
  end

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, room_path(conn, :index)
    assert json_response(conn, 200)["data"] == []
  end

  # test "shows chosen resource", %{conn: conn} do
  #   room = Repo.insert! %Room{}
  #   conn = get conn, room_path(conn, :show, room)
  #   assert json_response(conn, 200)["data"] == %{"id" => room.id,
  #     "name" => room.name,
  #     "topic" => room.topic}
  # end

  # test "renders page not found when id is nonexistent", %{conn: conn} do
  #   assert_error_sent 404, fn ->
  #     get conn, room_path(conn, :show, -1)
  #   end
  # end

  # test "creates and renders resource when data is valid", %{conn: conn} do
  #   conn = post conn, room_path(conn, :create), room: @valid_attrs
  #   assert json_response(conn, 201)["data"]["id"]
  #   assert Repo.get_by(Room, @valid_attrs)
  # end

  # test "does not create resource and renders errors when data is invalid", %{conn: conn} do
  #   conn = post conn, room_path(conn, :create), room: @invalid_attrs
  #   assert json_response(conn, 422)["errors"] != %{}
  # end

  # test "updates and renders chosen resource when data is valid", %{conn: conn} do
  #   room = Repo.insert! %Room{}
  #   conn = put conn, room_path(conn, :update, room), room: @valid_attrs
  #   assert json_response(conn, 200)["data"]["id"]
  #   assert Repo.get_by(Room, @valid_attrs)
  # end

  # test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
  #   room = Repo.insert! %Room{}
  #   conn = put conn, room_path(conn, :update, room), room: @invalid_attrs
  #   assert json_response(conn, 422)["errors"] != %{}
  # end

  # test "deletes chosen resource", %{conn: conn} do
  #   room = Repo.insert! %Room{}
  #   conn = delete conn, room_path(conn, :delete, room)
  #   assert response(conn, 204)
  #   refute Repo.get(Room, room.id)
  # end
end
