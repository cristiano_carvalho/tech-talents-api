defmodule TechTalents.UserTest do
  use TechTalents.ModelCase

  alias TechTalents.User

  @valid_attrs %{email: "cristiano.codelab@gmail.com", username: "cristiano23", password: "d3v3l0p3r"}
  @invalid_attrs %{}

  test "creates user with valid attributes" do
    changeset = User.registration_changeset(%User{}, @valid_attrs)
    assert changeset.valid?
  end

  test "do not create user with invalid attributes" do
    changeset = User.registration_changeset(%User{}, @invalid_attrs)
    refute changeset.valid?
  end

  test "do not create user when password is too short" do 
    changeset = User.registration_changeset(%User{}, 
      %{email: "cristiano.codelab@gmail.com", username: "cristiano23", password: "d3v"})
    refute changeset.valid?
  end
end
