defmodule TechTalents.UserRoomTest do
  use TechTalents.ModelCase

  alias TechTalents.UserRoom

  @valid_attrs %{user_id: 1, room_id: 1}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = UserRoom.changeset(%UserRoom{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = UserRoom.changeset(%UserRoom{}, @invalid_attrs)
    refute changeset.valid?
  end
end
