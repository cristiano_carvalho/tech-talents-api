defmodule TechTalents.Repo do
  use Ecto.Repo, otp_app: :tech_talents
  use Scrivener, page_size: 25
end
