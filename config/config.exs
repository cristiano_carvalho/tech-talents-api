# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :tech_talents,
  ecto_repos: [TechTalents.Repo]

# Configures the endpoint
config :tech_talents, TechTalents.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "xLlerELerxG9KDg33oxMBJUY/jPD7SH3tHR5VoeS8tm/eamwkKPbWaulzHYIedL5",
  render_errors: [view: TechTalents.ErrorView, accepts: ~w(json)],
  pubsub: [name: TechTalents.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.

config :guardian, Guardian,
  issuer: "TechTalents",
  ttl: {30, :days},
  verify_issuer: true,
  serializer: TechTalents.GuardianSerializer

config :guardian, Guardian,
  allowed_algos: ["HS512"], 
  verify_module: Guardian.JWT,  
  issuer: "TechTalents",
  ttl: { 30, :days },
  verify_issuer: true,
  secret_key: System.get_env("GUARDIAN_SECRET") || "LG17BzmhBeq81Yyyn6vH7GVdrCkQpLktol2vdXlBzkRRHpYsZwluKMG9r6fnu90m",
  serializer: TechTalents.GuardianSerializer

import_config "#{Mix.env}.exs"
